//
//  WeatherBuilder.swift
//  WeatherApi
//

import UIKit

struct WeatherBuilder {
    func build() -> UIViewController {
        let vc = WeatherVC()
        let interactor = WeatherInteractor(weatherRemoteAPI: WeatherFetcher())
        let presenter = WeatherPresenter(view: vc, interactor: interactor)
        vc.presenter = presenter
        return vc
    }
}
