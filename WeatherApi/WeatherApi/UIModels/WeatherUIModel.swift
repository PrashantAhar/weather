//
//  WeatherUIModel.swift
//  WeatherApi
//

import Foundation

// View Model for UI
struct WeatherUIModel {
    let city: CityUIModel
    let dayForecasts: [DayForecastUIModel]
    
    init(weather: WeatherResponse) {
        self.city = CityUIModel(name: weather.location?.name ?? "",
                                country: weather.location?.country ?? "",
                                dateTime: weather.location?.localtime ?? "")
        
        var forecasts = [DayForecastUIModel]()
        guard let forecastDays = weather.forecast?.forecastday else {
            self.dayForecasts = forecasts
            return
        }
        for forecast in forecastDays {
            let dayForecast = DayForecastUIModel(date: forecast.date,
                                                 icon: forecast.day.condition.icon,
                                                 minTemp: "MIN: \(forecast.day.mintempC)°C",
                                                 maxTemp: "MAX: \(forecast.day.maxtempC)°C")
            forecasts.append(dayForecast)
        }
        self.dayForecasts = forecasts
    }
}
