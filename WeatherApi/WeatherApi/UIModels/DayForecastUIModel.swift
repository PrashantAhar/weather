//
//  DayForecastUIModel.swift
//  WeatherApi
//

import Foundation

struct DayForecastUIModel {
    let date: String
    let icon: String
    let minTemp: String
    let maxTemp: String
    
    var displayIcon: String? {
        let url = URL(string: icon)
        let pathComponents = url?.pathComponents
        let lastPart = pathComponents?.suffix(2).joined(separator: "/")
        return lastPart
    }
    
    var displayDate: String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        guard let date = dateFormatter.date(from: date) else { return nil }
        dateFormatter.dateFormat = "dd MMM"
        let displayDate = dateFormatter.string(from: date)
        return displayDate
    }
}
