//
//  CityUIModel.swift
//  WeatherApi
//

import Foundation

struct CityUIModel {
    let name: String
    let country: String
    let dateTime: String
    
    var displayDateTime: String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        guard let date = dateFormatter.date(from: dateTime) else { return nil }
        dateFormatter.dateFormat = "dd MMM hh:mm a"
        let displayDate = dateFormatter.string(from: date)
        return displayDate
    }
}
