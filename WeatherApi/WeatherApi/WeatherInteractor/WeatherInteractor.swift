//
//  WeatherInteractor.swift
//  Weather
//

import Foundation
import Combine

protocol WeatherInteracting {
    func getWeatherListForSearch(city: String, completion: @escaping (WeatherResponse?) -> Void)
}

class WeatherInteractor: WeatherInteracting {
    let weatherRemoteAPI: WeatherRemoteAPI
    private var bag = [AnyCancellable]()
    
    init(weatherRemoteAPI: WeatherRemoteAPI) {
        self.weatherRemoteAPI = weatherRemoteAPI
    }
    
    func getWeatherListForSearch(city: String, completion: @escaping (WeatherResponse?) -> Void) {
        weatherRemoteAPI.getWeather(for: city)
            .receive(on: DispatchQueue.main)
            .sink { result in
                switch result {
                case .failure(let error):
                    print("Response from getWeather: \(error.localizedDescription)")
                    completion(nil)
                case .finished:
                    print("finished getWeather")
                }
            } receiveValue: { response in
                completion(response)
            }
            .store(in: &bag)
    }
}
