//
//  WeatherPresenter.swift
//  Weather
//

import Foundation

protocol WeatherPresentable {
    var view: WeatherViewing? { get }
    
    func viewDidLoad()
    func searchWeather(for city: String)
}

class WeatherPresenter: WeatherPresentable {
    weak var view: WeatherViewing?
    private let interactor: WeatherInteracting
    
    private enum Constants {
        static let network = NSLocalizedString("No response from the server. Please try again", comment: "")
        static let OK = NSLocalizedString("OK", comment: "")
        static let weatherResponse = "weatherResponse"
    }
    
    init (view: WeatherViewing, interactor: WeatherInteracting) {
        self.view = view
        self.interactor = interactor
    }
    
    func viewDidLoad() {
        getSavedResponseIfAny()
    }
    
    func searchWeather(for city: String) {
        interactor.getWeatherListForSearch(city: city) { [weak self] response in
            guard let response = response else {
                let alert = Alert(message: Constants.network, actionTitle: Constants.OK)
                self?.view?.displayAlert(alert)
                return
            }
            if let error = response.error {
                let alert = Alert(message: error.message, actionTitle: Constants.OK)
                self?.view?.displayAlert(alert)
            } else {
                self?.saveResponseForNextLaunch(response: response)
                self?.view?.displayWeather(self?.getWeatherUIModel(for: response))
            }
        }
    }
    
    private func saveResponseForNextLaunch(response: WeatherResponse) {
        let encodedData = try? JSONEncoder().encode(response)
        UserDefaults.standard.set(encodedData, forKey: Constants.weatherResponse)
    }
    
    private func getSavedResponseIfAny() {
        if let encodedData = UserDefaults.standard.data(forKey: Constants.weatherResponse),
           let weatherResponse = try? JSONDecoder().decode(WeatherResponse.self, from: encodedData) {
            view?.displayWeather(getWeatherUIModel(for: weatherResponse))
        }
    }
    
    // Converting API response to UI specific models
    private func getWeatherUIModel(for weatherResponse: WeatherResponse) -> WeatherUIModel {
        return WeatherUIModel(weather: weatherResponse)
    }
}
