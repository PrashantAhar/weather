//
//  Alert.swift
//  Weather
//

import Foundation

struct Alert {
    let message: String
    let actionTitle: String
}
