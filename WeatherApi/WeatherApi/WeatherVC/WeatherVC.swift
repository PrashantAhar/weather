//
//  WeatherVC.swift
//  WeatherApi
//

import UIKit

protocol WeatherViewing: AnyObject {
    var presenter: WeatherPresentable? { get }
    
    func displayWeather(_ weather: WeatherUIModel?)
    func displayAlert(_ alert: Alert)
}

class WeatherVC: UIViewController {
    
    // MARK: Properties
    var presenter: WeatherPresentable?
    private var tableView: UITableView!
    private var cityTextField: UITextField!
    private var activityIndicator: UIActivityIndicatorView!
    private var dataSource: WeatherUIModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        presenter?.viewDidLoad()
    }
    
    private func configureUI() {
        cityTextField = UITextField()
        cityTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: cityTextField.frame.height))
        cityTextField.leftViewMode = .always

        cityTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: cityTextField.frame.height))
        cityTextField.rightViewMode = .always

        cityTextField.placeholder = NSLocalizedString("Enter city name", comment: "")
        cityTextField.translatesAutoresizingMaskIntoConstraints = false
        cityTextField.delegate = self
        cityTextField.borderStyle = .none
        cityTextField.layer.cornerRadius = 3.0
        cityTextField.backgroundColor = .white
        cityTextField.returnKeyType = .search
        view.addSubview(cityTextField)
        
        tableView = UITableView(frame: view.bounds, style: .plain)
        tableView.keyboardDismissMode = .onDrag
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(CityCell.self, forCellReuseIdentifier: "CityCell")
        tableView.register(ForecastCell.self, forCellReuseIdentifier: "ForecastCell")
        tableView.dataSource = self
        tableView.delegate = self
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            cityTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8),
            cityTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            cityTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            cityTextField.heightAnchor.constraint(equalToConstant: 40),
            
            tableView.topAnchor.constraint(equalTo: cityTextField.bottomAnchor, constant: 16),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        view.backgroundColor = UIColor(named: "Background 1")!
    }
    
    private func hideLoader() {
        activityIndicator.removeFromSuperview()
    }
}

extension WeatherVC:  WeatherViewing {
    func displayWeather(_ weather: WeatherUIModel?) {
        hideLoader()
        dataSource = weather
        tableView.reloadData()
    }
    
    func displayAlert(_ alert: Alert) {
        hideLoader()
        let controller = UIAlertController(title: alert.message, message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: alert.actionTitle, style: .default, handler:  nil)
        controller.addAction(action)
        self.present(controller, animated: true, completion: nil)
    }
}

extension WeatherVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text, !text.isEmpty else { return true }
        
        textField.addSubview(activityIndicator)
        activityIndicator.frame = textField.bounds
        activityIndicator.startAnimating()
        
        presenter?.searchWeather(for: text)
        
        textField.text = nil
        textField.resignFirstResponder()
        return true
    }
}

extension WeatherVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard dataSource != nil else { return 0 }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return dataSource?.dayForecasts.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath) as! CityCell
            cell.configure(with: dataSource?.city)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastCell", for: indexPath) as! ForecastCell
            let forecast = dataSource?.dayForecasts[indexPath.row]
            cell.configure(with: forecast)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        100.0
    }
}
