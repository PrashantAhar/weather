//
//  EndPoint.swift
//

import Foundation

enum WeatherEndpoint: EndPointType {
    case getWeather(query: String)

    var baseURL: URL { URL(string: "https://api.weatherapi.com/v1/")! }

    static let key = "522db6a157a748e2996212343221502"

    var path: String {
        switch self {
        case .getWeather:
            return "forecast.json"
        }
    }

    var httpMethod: HTTPMethod {
        switch self {
        case .getWeather:
            return .get
        }
    }

    var task: HTTPTask {
        switch self {
        case .getWeather(let query):
            let urlParams = [
                "key": Self.key,
                "q": query,
                "days": "14",
                "aqi": "no",
                "alerts": "no"
            ]
            return .requestParameters(bodyParameters: nil, urlParameters: urlParams)
        }
    }

    var headers: HTTPHeaders? { nil }
}
