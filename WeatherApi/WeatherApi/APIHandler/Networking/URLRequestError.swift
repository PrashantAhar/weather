//
//  URLRequestError.swift
// 

import Foundation

enum URLRequestError: Error {
    case badRequest
    case internalServerError
    case outdated
    case unknown
    case unauthorized
    case unexpectedStatusCode
}
