//
//  NetworkHeaders.swift
//

import Foundation

class NetworkHeaders {
    static let contentType = "Content-type"
    static let applicationJson = "application/json"
    static let authorization = "Authorization"
}
