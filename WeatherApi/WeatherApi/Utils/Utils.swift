//
//  Utils.swift
//  WeatherApi
//

import UIKit

extension UILabel {
    static func createLabel(ofSize: CGFloat = 10, textColor: UIColor = .white) -> Self {
        let label = Self()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: ofSize)
        label.textColor = textColor
        return label
    }
}
