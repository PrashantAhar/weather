//
//  WeatherVCTests.swift
//  WeatherApiTests
//

import XCTest
@testable import WeatherApi

class WeatherVCTests: XCTestCase {
    
    private var sut: WeatherVC!
    private var presenterMock: WeatherPresenterMock!
    
    override func setUp() {
        super.setUp()
        presenterMock = WeatherPresenterMock()
        sut = WeatherVC()
        sut.presenter = presenterMock
    }
    
    override func tearDown() {
        super.tearDown()
        presenterMock = nil
        sut = nil
    }
    
    func test_viewDidLoad() {
        let _ = sut.view
        
        sut.viewDidLoad()
        
        guard case .viewDidLoad = presenterMock.calledMethods.first else {
            XCTFail("viewDidLoad not called on presenter but \(String(describing: presenterMock.calledMethods.first))")
            return
        }
    }
    
    func test_searchCityWeather() {
        test_viewDidLoad()
        presenterMock.resetCalledMethods()
        
        let textField = UITextField()
        textField.text = "New York"
        _ = sut.textFieldShouldReturn(textField)
        
        guard case let .searchWeather(city: city) = presenterMock.calledMethods.first else {
            XCTFail("searchWeather is not called on presenter but \(String(describing: presenterMock.calledMethods.first))")
            return
        }
        
        XCTAssertEqual(city, "New York")
    }
}
