//
//  WeatherApiTests.swift
//  WeatherApiTests
//

import XCTest
@testable import WeatherApi

class WeatherPresenterTests: XCTestCase {
    
    private var sut: WeatherPresenter!
    private var interactorMock: WeatherInteractorMock!
    private var viewMock: WeatherViewMock!
    
    override func setUp() {
        super.setUp()
        interactorMock = WeatherInteractorMock()
        viewMock = WeatherViewMock()
        sut = WeatherPresenter(view: viewMock, interactor: interactorMock)
    }
    
    override func tearDown() {
        super.tearDown()
        interactorMock = nil
        viewMock = nil
        sut = nil
    }
    
    func test_viewDidLoad() {
        sut.viewDidLoad()
        
        guard getWeatherSuccessResponse() != nil else {
            XCTFail("Weather cannot be nil in testing")
            return
        }
        if let _ = UserDefaults.standard.data(forKey: "weatherResponse") {
            guard case let .displayWeather(weather: uiModel) = viewMock.calledMethods.first else {
                XCTFail("displayWeather not called on viewMock")
                return
            }
            XCTAssertNotNil(uiModel?.city)
            XCTAssertNotNil(uiModel?.dayForecasts)
        }
    }
    
    func test_searchWeatherForSuccess() {
        let city = "New Delhi"
        
        sut.searchWeather(for: city)
        
        guard case let .getWeatherListForSearch(city: calledCity, completion: completion) = interactorMock.calledMethods.first else {
            XCTFail("getWeatherListForSearch is not called on interactorMock")
            return
        }
        XCTAssertEqual(city, calledCity)
        
        guard let weatherResponse = getWeatherSuccessResponse() else {
            XCTFail("Weather cannot be nil in testing")
            return
        }
        
        // success
        completion(weatherResponse)
        
        guard case let .displayWeather(weather: uiModel) = viewMock.calledMethods.first else {
            XCTFail("displayWeather is not called on viewMock")
            return
        }
        XCTAssertEqual(uiModel?.city.name, "New Delhi")
    }
    
    func test_searchWeatherWithFailure() {
        let city = ""
        
        sut.searchWeather(for: city)
        guard case let .getWeatherListForSearch(city: _, completion: completion) = interactorMock.calledMethods.first else {
            XCTFail("getWeatherListForSearch is not called on interactorMock")
            return
        }
        // failure case
        completion(nil)
        guard case let .displayAlert(alert: alert) = viewMock.calledMethods.first else {
            XCTFail("displayAlert is not called on viewMock")
            return
        }
        
        XCTAssertEqual(alert.message,
                       NSLocalizedString("No response from the server. Please try again", comment: ""))
        
    }
    
    func test_noLocationFoundError() {
        sut.searchWeather(for: "No Location")
        guard case let .getWeatherListForSearch(city: _, completion: completion) = interactorMock.calledMethods.first else {
            XCTFail("getWeatherListForSearch is not called on interactorMock")
            return
        }
        
        guard let weatherError = getWeatherErrorResponse() else {
            XCTFail("Error cannot be nil in testing")
            return
        }
        
        XCTAssertEqual(weatherError.error?.code ?? 0, 1006)
        
        // success with weather error
        completion(weatherError)
        
        guard case let .displayAlert(alert: alert) = viewMock.calledMethods.first else {
            XCTFail("displayAlert is not called on viewMock")
            return
        }
        
        XCTAssertEqual(alert.message, "No matching location found.")
    }
}
