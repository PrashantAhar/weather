//
//  Mocks.swift
//  WeatherApiTests
//

import Foundation
import XCTest
@testable import WeatherApi

class WeatherInteractorMock: WeatherInteracting {
    enum MethodHandler {
        case getWeatherListForSearch(city: String, completion: (WeatherResponse?) -> Void)
    }
    
    private(set) var calledMethods: [MethodHandler] = []
    
    func resetCalledMethods() {
        calledMethods = []
    }
    
    func getWeatherListForSearch(city: String, completion: @escaping (WeatherResponse?) -> Void) {
        calledMethods.append(.getWeatherListForSearch(city: city, completion: completion))
    }
}

class WeatherViewMock: WeatherViewing {
    enum MethodHandler {
        case displayAlert(alert: Alert)
        case displayWeather(weather: WeatherUIModel?)
    }
    
    private(set) var calledMethods: [MethodHandler] = []
    
    func resetCalledMethods() {
        calledMethods = []
    }
    
    var presenter: WeatherPresentable?
    
    func displayAlert(_ alert: Alert) {
        calledMethods.append(.displayAlert(alert: alert))
    }
    
    func displayWeather(_ weather: WeatherUIModel?) {
        calledMethods.append(.displayWeather(weather: weather))
    }
}

class WeatherPresenterMock: WeatherPresentable {
    enum MethodHandler {
        case viewDidLoad
        case searchWeather(city: String)
    }
    
    private(set) var calledMethods: [MethodHandler] = []
    
    func resetCalledMethods() {
        calledMethods = []
    }

    var view: WeatherViewing?
    
    func viewDidLoad() {
        calledMethods.append(.viewDidLoad)
    }
    
    func searchWeather(for city: String) {
        calledMethods.append(.searchWeather(city: city))
    }
}

let responseJsonString = """
{
    "location": {
        "name": "New Delhi",
        "region": "Delhi",
        "country": "India",
        "localtimeEpoch": 1674297012,
        "localtime": "2023-01-21 16:00"
    },
    "current": {
        "tempC": 21.0,
        "tempF": 69.8,
        "isDay": 1,
        "condition": {
            "text": "Overcast",
            "icon": "//cdn.weatherapi.com/weather/64x64/day/122.png",
            "code": 1009
        }
    },
    "forecast": {
        "forecastday": [
            {
                "date": "2023-01-21",
                "dateEpoch": 1674259200,
                "day": {
                    "maxtempC": 24.9,
                    "mintempC": 11.4,
                    "condition": {
                        "text": "Sunny",
                        "icon": "//cdn.weatherapi.com/weather/64x64/day/113.png",
                        "code": 1000
                    }
                }
            }
        ]
    }
}
"""

let errorJsonString = """
{
    "error": {
        "code": 1006,
        "message": "No matching location found."
    }
}
"""

extension XCTestCase {
    func getWeatherSuccessResponse() -> WeatherResponse? {
        if let jsonData = responseJsonString.data(using: .utf8) {
            do {
                let decoder = JSONDecoder()
                let weather = try decoder.decode(WeatherResponse.self, from: jsonData)
                return weather
            } catch let DecodingError.dataCorrupted(context) {
                print(context)
            } catch let DecodingError.keyNotFound(key, context) {
                print("Key '\(key)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch let DecodingError.valueNotFound(value, context) {
                print("Value '\(value)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch let DecodingError.typeMismatch(type, context)  {
                print("Type '\(type)' mismatch:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch {
                XCTFail("Dummy Weather Error not generated")
                return nil
            }
        }
        return nil
    }
    
    func getWeatherErrorResponse() -> WeatherResponse? {
        if let jsonData = errorJsonString.data(using: .utf8) {
            do {
                let decoder = JSONDecoder()
                let weather = try decoder.decode(WeatherResponse.self, from: jsonData)
                return weather
            } catch let DecodingError.dataCorrupted(context) {
                print(context)
            } catch let DecodingError.keyNotFound(key, context) {
                print("Key '\(key)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch let DecodingError.valueNotFound(value, context) {
                print("Value '\(value)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch let DecodingError.typeMismatch(type, context)  {
                print("Type '\(type)' mismatch:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch {
                XCTFail("Dummy Weather Response not generated")
                return nil
            }
        }
        return nil
    }
}
